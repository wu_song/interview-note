# InterviewNote

##  😜介绍
大家好，我是路人zhang，毕业于西电通信工程专业，经过秋招中几个月的面试，饱受Java八股文的折磨，遂将常见面试题总结成了面试手册，其中的内容都是我精心总结的，不是在网上随意复制粘贴而来的，欢迎大家多帮忙转给身边的小伙伴。面试手册的内容都是首发在**公众号"路人zhang"**，隔一段时间会整理到面试手册中，大家可以关注公众号查看最新内容。如果在秋招中遇到任何问题，也可以在公众号加我微信私聊。


<p align="center">
    <br>
    <a href="./images/加群.jpg"><img src="https://img.shields.io/badge/WeChat-微信交流群-blue.svg" alt="微信交流群"></a>
  <a href="https://www.zhihu.com/people/xing-xing-dian-kong"><img src="https://img.shields.io/badge/知乎-路人zhang-green.svg" alt="知乎"></a>
   <a href="https://blog.csdn.net/zydybaby/article/details/109701816"><img src="https://img.shields.io/badge/CSDN-路人zhang-important.svg" alt="CSDN"></a>
       <a href="https://www.nowcoder.com/profile/115285789"><img src="https://img.shields.io/badge/牛客网-路人zhang-9cf" alt="牛客网"></a>
        </p>

## :100: 目录结构（持续更新中）

以下链接中的文章均带目录，比较方便阅读

### 经典电子书

* [最全的免费电子书合集](https://gitee.com/lurenzhang888/cs-books)

### Java基础

* [Java高频面试题合集背诵版](https://blog.csdn.net/zydybaby/article/details/109701816)

### 集合

- [一文搞懂所有HashMap面试题](https://blog.csdn.net/zydybaby/article/details/110191231)

* [Java集合高频面试题合集背诵版](https://blog.csdn.net/zydybaby/article/details/110183091)

### Java并发

* [Java并发编程高频面试题第一弹](https://blog.csdn.net/zydybaby/article/details/116148366)
* [面试官：请用五种方法实现多线程交替打印问题](https://blog.csdn.net/zydybaby/article/details/116454944)
* [面试官：请详细说下synchronized的实现原理](https://blog.csdn.net/zydybaby/article/details/117453836)
* [面试官：请说下volatile的实现原理](https://blog.csdn.net/zydybaby/article/details/117791215)
* [Java并发编程高频面试题第二弹](https://blog.csdn.net/zydybaby/article/details/118945973)
* [Java并发编程面试合集背诵版](https://zhuanlan.zhihu.com/p/401221052)

### MySQL

* [数据库索引高频面试题](https://blog.csdn.net/zydybaby/article/details/111886554)
* [MySQL的事务和锁高频面试题](https://blog.csdn.net/zydybaby/article/details/112223220)
* [MySQL数据库优化高频面试题](https://blog.csdn.net/zydybaby/article/details/112971071)
* [MySQL高频面试题合集背诵版](https://blog.csdn.net/zydybaby/article/details/112971407)

### 计算机网络

* [一文搞定所有TCP/HTTP面试题](https://blog.csdn.net/zydybaby/article/details/111355640)
* [计算机网络高频面试题合集背诵版](https://blog.csdn.net/zydybaby/article/details/111356451)

### 大厂面经系列
* [美团成都一面面经及详细答案](https://zhuanlan.zhihu.com/p/407099395)
* [美团面经及详细答案](https://zhuanlan.zhihu.com/p/411184440)

### 面试中常见的智力题

* [字节面试常考的智力题你会几道？](https://zhuanlan.zhihu.com/p/395132338)
* [字节最爱问的智力题，你会几道？（二）](https://zhuanlan.zhihu.com/p/397235208)
* [字节最爱问的智力题，你会几道？（三）](https://zhuanlan.zhihu.com/p/425152611)

### 程序员应如何写简历

- [程序员应如何写简历](https://zhuanlan.zhihu.com/p/386236818)

- [优秀简历模板](https://mp.weixin.qq.com/s/aFevmHa7xuTGE_4IewTdUQ)

### 我的校招总结

- [2021秋招总结](https://www.zhihu.com/question/386930877/answer/1660562121)

### Java学习路线（知乎1000多人收藏）

- [Java学习路线](https://zhuanlan.zhihu.com/p/366455934)

### 程序人生

- [谈谈程序员的发展路线和北京户口问题](https://zhuanlan.zhihu.com/p/375980526)

- [一些工作上的建议](https://mp.weixin.qq.com/s/XpVMCVrM-1uZFeMvvlqRPQ)

### 面试手册PDF下载

在公众号 **路人zhang** 回复“面试手册”即可
### 联系我
公众号：路人zhang   微信号：lurenzhang888
<p align="center">
<a target="_blank" href="">
<img src="./images/gitee_微信公众号.jpg" style="width:200px" />
<img src="./images/gitee_微信号.jpg" style="width:200px" /><br>
</p>